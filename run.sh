#!/bin/bash
usage="run.sh <sourceDirectory>"
verbose=1
if [ $# -ne 1 ]
then
    echo $usage
    exit 1
fi
src=$1
if ! cd $src
then
    exit 1
fi
for f in Main.java Evenement.java Echeancier.java
do
    if ! cat $f > /dev/null
    then
	echo No file ${src}/${f}
	exit 1
    fi
done
if ! rm -f *.class
then
    exit 1
fi
echo ----------
echo $(grep assert *.java | wc -l) asserts 
echo ----------
echo $(grep notYetImplemented *.java | wc -l) notYetImplemented
echo ----------
echo -n 'recompile ' 
if javac -encoding UTF-8 *.java >& /dev/null
then
    echo '(UTF-8) good'
elif javac -encoding ASCII *.java >& /dev/null
then
    echo '(ASCII) strange...'
elif javac -encoding ISO-8859 *.java >& /dev/null
then
    echo '(ISO-8859) windows... beurk'
else
    echo erreur: \(cd ${d} \; javac -encoding UTF-8 \*.java\)
    exit 1
fi
if cat ~colnet5/IO/input.1 >& /dev/null
then
    IO=~colnet5/IO
elif cat ~/IO/input.1 >& /dev/null
then
    IO=~/IO
else
    IO=$(find ~ -type d -name 'IO')
fi
if ! cat ${IO}/input.1 > /dev/null
then
    echo no IO directory
    exit 1
fi
for t in {1..16}
do
    echo ----------
    if ! cat ${IO}/output.${t} 1> /dev/null
    then
	echo ${IO}/output.${t} not yet done
	exit 1
    fi
    if ! java -ea Main < ${IO}/input.${t} >& /tmp/output.${t}
    then
	echo output.${t} ' (assert or notYetImplemented)'
	tail /tmp/output.${t}
	echo tail /tmp/output.${t}
	exit 1
    fi
    if ! diff ${IO}/output.${t} /tmp/output.${t} >& /dev/null
    then
	echo output.${t} ' (wrong result)'
	diff ${IO}/output.${t} /tmp/output.${t} | head 
	echo "diff ${IO}/output.${t} /tmp/output.${t} | head" 
	exit 1
    fi
    echo output.${t} good
done
echo ----------
echo 'Veeerrryy Goood !'
exit 0

