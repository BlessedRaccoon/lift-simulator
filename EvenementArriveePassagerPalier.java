/*
 * APP: Arrivée Passager Palier L'instant précis ou un nouveau passager arrive
 * sur un palier donné, dans le but de monter dans la cabine.
 */
public class EvenementArriveePassagerPalier extends Evenement {

	private Etage étage;

	public EvenementArriveePassagerPalier(long d, Etage edd) {
		super(d);
		étage = edd;
	}

	public void afficheDetails(StringBuilder buffer, Immeuble immeuble) {
		buffer.append("APP ");
		buffer.append(étage.numéro());
	}

	public void traiter(Immeuble immeuble, Echeancier echeancier) {
		assert étage != null;
		assert immeuble.étage(étage.numéro()) == étage;
		Passager p = new Passager(date, étage, immeuble);
		Cabine c = immeuble.cabine;

		if (c.étage == étage) {
			if (c.porteOuverte) {
				if (c.intention() == '-') {
					// notYetImplemented();
					c.changerIntention(p.sens());
					echeancier.ajouter(new EvenementFermeturePorteCabine(date + tempsPourOuvrirOuFermerLesPortes));
					char fmp = c.faireMonterPassager(p);
					
					if (fmp == 'O') {
						assert true;
					} else {
						assert false : "else impossible";
					}
				} else {
					notYetImplemented();
				}
			}
			else {

			}
		} else {
			if (c.estVide()) {
				if (p.étageDépart().numéro() < c.étage.numéro()) {
					c.changerIntention('v');
				} else {
					c.changerIntention('^');
				}
				étage.ajouter(p);
				echeancier.ajouter(new EvenementFermeturePorteCabine(date + tempsPourOuvrirOuFermerLesPortes));
			}
			else {
				notYetImplemented();
			}

			// Bizarre qu'il ne faille pas utiliser ça comme étage pour le PAP
			// A vérifier peut-être un jour
			// final Etage etageSuivant = (p.étageDestination().numéro() < étage.numéro()) ? immeuble.étage(étage.numéro() + 1) : immeuble.étage(étage.numéro() - 1);

			echeancier.ajouter(new EvenementPietonArrivePalier(date + délaiDePatienceAvantSportif, étage, p));
		}
		
		echeancier.ajouter(new EvenementArriveePassagerPalier(date + étage.arrivéeSuivante(), étage));

		assert c.intention() != '-' : "intention impossible";
	}

}
