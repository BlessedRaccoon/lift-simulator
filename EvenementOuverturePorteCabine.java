/**
 * OPC: Ouverture Porte Cabine L'instant précis ou la porte termine de s'ouvrir.
 */
public class EvenementOuverturePorteCabine extends Evenement {

	public EvenementOuverturePorteCabine(long d) {
		super(d);
	}

	public void afficheDetails(StringBuilder buffer, Immeuble immeuble) {
		buffer.append("OPC");
	}

	public void traiter(Immeuble immeuble, Echeancier echeancier) {
		Cabine cabine = immeuble.cabine;
		Etage étage = cabine.étage;
		assert !cabine.porteOuverte;
		
		cabine.porteOuverte = true;
		
		cabine.faireDescendrePassagers(immeuble, date);
		if (étage.aDesPassagers()) {
			for(Passager p : étage.getPassagers()) {
				cabine.faireMonterPassager(p);
				étage.enlever(p);
			}
			echeancier.ajouter(new EvenementFermeturePorteCabine(date + tempsPourOuvrirOuFermerLesPortes));
		}
		else if ( (!immeuble.passagerAuDessus(étage)) && (!immeuble.passagerEnDessous(étage))) {
			cabine.changerIntention('-');
		}

		assert cabine.porteOuverte;
	}

}
